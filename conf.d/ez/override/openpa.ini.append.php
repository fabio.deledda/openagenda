<?php /* #?ini charset="utf-8"?

[NetworkSettings]
PrototypeUrl=http://saasopenpa.opencontent.it/openpa/classdefinition/

[InstanceSettings]
LiveIPList[]

[OpenpaAgendaPushSettings]
AvailableEndpoint[]

[InstanceSettings]
NomeAmministrazioneAfferente=OpenContent OpenAgenda
UrlAmministrazioneAfferente=https://www.opencontent.it/openagenda

[CreditsSettings]
Title=OpenAgenda
Name=OpenAgenda
Url=https://www.opencontent.it/openagenda
CodeVersion=2.0.0
CodeUrl=https://gitlab.com/opencontent/openagenda

*/?>
