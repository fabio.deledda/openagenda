<?php /* #?ini charset="utf-8"?

[RemoveSettings]
MaxNodesRemoveSubtree=50

[VersionManagement]
DefaultVersionHistoryLimit=10
VersionHistoryClass[]
VersionHistoryClass[survey]=1000

[RelationAssignmentSettings]
ClassSpecificAssignment[]=file_pdf;media/files

[CustomTagSettings]
AvailableCustomTags[]=underline
AvailableCustomTags[]=acronimo
AvailableCustomTags[]=abbreviazione
IsInline[underline]=true
IsInline[acronimo]=true
IsInline[abbreviazione]=true
AvailableCustomTags[]=iframe

[iframe]
CustomAttributes[]=src
CustomAttributes[]=width
CustomAttributes[]=height
CustomAttributes[]=name
CustomAttributes[]=scrolling
CustomAttributes[]=frameborder
CustomAttributesDefaults[width]=330
CustomAttributesDefaults[height]=400
CustomAttributesDefaults[name]=embedded_iframe
CustomAttributesDefaults[scrolling]=auto
CustomAttributesDefaults[frameborder]=0

[acronimo]
CustomAttributes[]
CustomAttributes[]=sigla
CustomAttributes[]=lingua
CustomAttributesDefaults[lingua]=it

[abbreviazione]
CustomAttributes[]
CustomAttributes[]=sigla
CustomAttributes[]=lingua
CustomAttributesDefaults[lingua]=it

[factbox]
CustomAttributes[]=align
CustomAttributes[]=title
CustomAttributesDefaults[align]=right
CustomAttributesDefaults[title]=factbox

[table]
AvailableClasses[]=list
AvailableClasses[]=cols
AvailableClasses[]=comparison
AvailableClasses[]=default
CustomAttributes[]=summary
CustomAttributes[]=caption
ClassDescription[list]=List
ClassDescription[cols]=Timetable
ClassDescription[comparison]=Comparison Table
ClassDescription[default]=Default
Defaults[rows]=2
Defaults[cols]=2
Defaults[width]=100%
Defaults[border]=0
Defaults[class]=default

[td]
CustomAttributes[]=valign

[th]
CustomAttributes[]=scope
CustomAttributes[]=abbr
CustomAttributes[]=valign

[object]
AvailableClasses[]=itemized_sub_items
AvailableClasses[]=itemized_subtree_items
AvailableClasses[]=highlighted_object
AvailableClasses[]=vertically_listed_sub_items
AvailableClasses[]=horizontally_listed_sub_items
CustomAttributes[]=offset
CustomAttributes[]=limit
ClassDescription[itemized_sub_items]=Itemized Sub Items
ClassDescription[itemized_subtree_items]=Itemized Subtree Items
ClassDescription[highlighted_object]=Highlighted Object
ClassDescription[vertically_listed_sub_items]=Vertically Listed Sub Items
ClassDescription[horizontally_listed_sub_items]=Horizontally Listed Sub Items
CustomAttributesDefaults[offset]=0
CustomAttributesDefaults[limit]=5

[embed]
AvailableClasses[]=itemized_sub_items
AvailableClasses[]=itemized_subtree_items
AvailableClasses[]=highlighted_object
AvailableClasses[]=vertically_listed_sub_items
AvailableClasses[]=horizontally_listed_sub_items
CustomAttributes[]=offset
CustomAttributes[]=limit
ClassDescription[itemized_sub_items]=Itemized Sub Items
ClassDescription[itemized_subtree_items]=Itemized Subtree Items
ClassDescription[highlighted_object]=Highlighted Object
ClassDescription[vertically_listed_sub_items]=Vertically Listed Sub Items
ClassDescription[horizontally_listed_sub_items]=Horizontally Listed Sub Items
CustomAttributesDefaults[offset]=0
CustomAttributesDefaults[limit]=5

[quote]
CustomAttributes[]=align
CustomAttributes[]=author
CustomAttributesDefaults[align]=right
CustomAttributesDefaults[autor]=Quote author

[embed-type_images]
AvailableClasses[]

[literal]
AvailableClasses[]
AvailableClasses[]=html
*/ ?>
