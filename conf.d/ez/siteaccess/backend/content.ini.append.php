<?php /* #?ini charset="utf-8"?

[EditSettings]
AdditionalTemplates[]=load_ocopendata_forms.tpl

[ClassAttributeSettings]
CategoryList[content]=Main contents
CategoryList[meta]=Meta
CategoryList[trasparenza]=Trasparency
CategoryList[hidden]=Hidden
CategoryList[details]=Other contents
CategoryList[taxonomy]=Category
CategoryList[attribuzione]=Attribution
CategoryList[geo]=Where
CategoryList[time]=When
CategoryList[booking_description]=Activity presentation
CategoryList[booking_stuff]=Stuff
CategoryList[booking_services]=Communication
CategoryList[booking_definitions]=Booking
CategoryList[config]=Configurations
CategoryList[moderation]=Moderation
CategoryList[controls]=Abilitations
CategoryList[date]=Temporal references
CategoryList[altre_info]=More info

[embed]
AvailableViewModes[]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
AvailableViewModes[]=banner
AvailableClasses[]
AvailableClasses[]=primary
AvailableClasses[]=secondary
AvailableClasses[]=success
AvailableClasses[]=info
AvailableClasses[]=warning
AvailableClasses[]=danger
AvailableClasses[]=dark
AvailableClasses[]=black

[embed-inline]
AvailableViewModes[]
AvailableViewModes[]=embed-inline

*/
