# OpenAgenda

[![pipeline status](https://gitlab.com/opencontent/openagenda/badges/master/pipeline.svg)](https://gitlab.com/opencontent/openagenda/commits/master)
[![license](https://img.shields.io/badge/license-GPL-blue.svg)](https://gitlab.com/opencontent/openagenda/blob/master/LICENSE)

Calendario degli eventi partecipato dai cittadini

## Descrizione

![Homepage di OpenAgenda](https://www.opencontent.it/var/opencontent/storage/images/media/images/come-si-presenta-il-calendario-al-cittadino/2088-1-ita-IT/Come-si-presenta-il-calendario-al-cittadino_imagefullwide.png)
E’ un calendario eventi partecipato dai soggetti che animano la vita culturale 
e sociale di una comunità locale; associazioni, biblioteche e musei pubblicano 
i propri eventi in un unico calendario che ne facilita la diffusione multicanale, 
in coordinamento con l'ente locale; un modo per rafforzare le relazioni tra
amministrazione e territorio, secondo il paradigma dell'*OpenGovernment*.

Consente di prevenire le sovrapposizioni di date e di raggruppare le iniziative 
per tematiche in base agli interessi di cittadini e turisti, che vi accedono 
tramite pc, tablet, smartphone, o scaricando una locandina sintetica corredata 
di un QRcode per evento, generata automaticamente. 
I profili delle associazioni e gli stessi eventi vengono rilasciati come *Open Data*
e resi disponibili via API.

## Altri riferimenti

Per maggiori informazioni è possibile consultare: 

 * [Demo](https://openagenda.openpa.opencontent.io)
 * [Pagina ufficiale del prodotto](https://www.opencontent.it/Per-la-PA/OpenAgenda)
 * [Manuale utente](https://manuale-openagenda.readthedocs.io)

## API 

 * [API della Demo](https://openagenda.openpa.opencontent.io/api/opendata/)
 * [Documentazione API](https://documenter.getpostman.com/view/7046499/S17tPncK?version=latest)


## Project Status

Il prodotto è *stabile* e *production ready* e usato in produzione in diverse città Italiane. Lo sviluppo avviene in modo costante, sia su richiesta degli Enti utilizzatori, sia su iniziativa autonoma del _maintainer_.

## Informazioni tecniche

L'applicazione è sviluppata sul CMS Ez Publish e consta di codice PHP, il dump del database contenente
una installazione pienamente funzionante dell'applicativo e i file di configurazione necessari per 
il motore di ricerca.

### Struttura del Repository

Il repository contiene i seguenti file:
```
composer.json
composer.lock
```
Il repository non contiene direttamente il codice applicativo, contiene invece il file delle dipendenze PHP
che elenca tutti i componenti necessari all'applicazione: il CMS Ez Publish, le estensioni dello stesso utilizzate
e tra queste l'estensione openagenda che implementa le funzionalità più rilevanti. Con un comando `composer install` dalla stessa directory è possibile ottenere il codice dell'applicativo pronto all'uso (si veda anche [Dockerfile, L128](https://gitlab.com/opencontent/openagenda/blob/master/Dockerfile#L128))

Le directory
```
/dfs/var/prototipo
/var
```
contengono invece le risorse statiche minime necessarie all'applicazione (loghi, elementi grafici generali, etc...).


```
/sql
```
La directory SQL contiene un dump del database PostgreSQL configurato e pronto all'uso.

Il file `Dockerfile` e le directory
```
/scripts
/conf.d
```
contengono file di supporto per la creazione dell'immagine Docker dell'applicativo.

Il file
```
docker-compose.yml
```
contiene i servizi minimi necessari per l'esecuzione di OpenAgenda e può essere usato per il deploy su un singolo host o su un cluster Docker Swarm o Kubernetes.

Il file
```
docker-compose.override.yml
```
viene usato automaticamente da docker-compose quando e' disponibile: quando si clona il repository ad esempio e si hanno tutti i file a disposizione, questo aggiunge alcuni servizi di supporto utili in ambiente di sviluppo, come una interfaccia web per il database.


### Requisiti

Lo stack minimo di OpenAgenda è il seguente:
  * database PostgreSQL 9.6
  * PHP 7.2
  * Solr 4.10.x per il motore di ricerca

Nel file `docker-compose.yml` vengono inoltre utilizzati:
  * Varnish per la cache dei contenuti
  * Traefik per il routing tra i diversi container

Nel `Dockerfile` è possibile trovare tutte le dipendenze dal sistema operativo del linguaggio PHP.

### Come si esegue la build del repository

Con il comando che segue:

    docker build -t openagenda .

si esegue la build dell'immagine docker dell'applicazione: vengono installate le dipendenze
del sistema operativo e successivamente composer e le dipendenze applicative.

### Local docker environment

Per avere un ambiente completo di demo o di sviluppo clonare
il repository e dalla directory principale dare i comandi:

```
docker-compose up -d
```

Il prototipo di OpenAgenda sarà disponibile ai seguenti indirizzi:

* [openagenda.localtest.me](https://openagenda.localtest.me)
* [openagenda.localtest.me/backend](https://openagenda.localtest.me/backend), per l'area riservata ai redattori e amministratori (l'utente di default con cui si accede è `admin/admin`)
* [solr.localtest.me/solr](https://solr.localtest.me/solr), per l'interfaccia amministrativa di SOLR
* [traefik.localtest.me:8080](https://traefik.localtest.me:8080) per l'interfaccia amministrativa di Traefik
* [Mailhog](https://mailhog.openagenda.localtest.me/), le email inviate in questo ambiente sono visibili in questa interfaccia web
* [PgWeb](https://pgweb.openagenda.localtest.me/), interfaccia web per il database PostgreSQL

Il certificato usato per tutti i domini è un wildcard self-signed certificate sul dominio *.localtest.me, quindi al primo collegamento si ottiene un warning.

### Useful commands

Per vedere i log di tutti container:

    docker-compose logs -f --tail 20

o solo uno di essi (ad esempio php):

    docker-compose logs -f --tail 20 php

Per ottenere una shell all'interno del container PHP, eseguire:

    docker-compose exec php bash

Per accedere al database da CLI:

    docker-compose exec postgres bash
    bash-4.4# psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB"


### Rebuild database from scratch

Per ricreare il database dai dump del repository e tornare quindi al prototipo orginale è necessario spegnere il db, 
rimuovere il volume del container db e riavviare il database

    docker-compose stop postgres; \
    docker-compose rm postgres; \
    docker volume rm openagenda_pgdata; \
    docker-compose up -d postgres

Attenzione: il nome del volume dipende dalla directory in cui avete fatto il clone, se non è `openagenda` è necessario adattare il comando precedente.

### Continuous Integration

Il software ha una pipeline di CI, che esegue una build ad ogni commit, disponibile alla seguente url:

https://gitlab.com/opencontent/openagenda/pipelines

Le build delle immagini docker sono disponibili nel [Registry](https://gitlab.com/opencontent/openagenda/container_registry) di GitLab.

### Deploy in produzione

Per il deploy in un ambiente di produzione è necessario configurare diversi elementi: in generale
tutte le configurazioni si trovano nei file `conf.d/ez/override/<file>.ini.append.php`, si indicano comunque
di seguito alcuni punti di sicuro interesse. 
In particolare sarà necessario intervenire su:
  * [Dominio](/conf.d/ez/override/site.ini.append.php#L96-97)
  * [Server di posta](/conf.d/ez/override/site.ini.append.php#L103-113)

## Copyright (C)

Il titolare del software è il [Comune di Ala](https://www.comune.ala.tn.it), che ne ha richiesto la realizzazione a Opencontent
per il sito [ViviAla](https://www.comune.ala.tn.it/eventi).

Il software è rilasciato con licenza aperta ai sensi dell'art. 69 comma 1 del [Codice dell’Amministrazione Digitale](https://cad.readthedocs.io/)

### Maintainer

[Opencontent SCARL](https://www.opencontent.it/), è responsabile della progettazione, realizzazione e manutenzione tecnica di OpenAgenda

